# S3 Buckets Management with Security using Ansible

A reference implementation to create AWS S3 buckets and secure them using Ansible framework

# Pre-requirements - Already installed in your local environment

To use this project we assume following dependencies already installed or configured

1. Python is already installed. This project codebase is developed on Python 3.9.6

# Pre-requirements - Additional Commands in your local environment

Execute below commands in your local environment to use this project code

NOTE:

1. requirements.txt file is from this project codebase

2. requirements.yml file is from this project codebase and contains Ansible Collections required for this project codebase

```sh

    # Below is optional and required only if below ansible-galaxy command
    # throws an error like "....get local issuer certificate (_ssl.c:1129)>"
    # This is on Mac OS and I have Python 3.9 installed in my local environment
    # at the time of this development
    # cd /Applications/Python\ 3.9/ ./Install\ Certificates.command

    # You need to run below command first because the python module dependencies are
    # not installed by ansible-galaxy.
    python -m pip install -r requirements.txt

    ansible-galaxy install -r requirements.yml
    # You should see a message "amazon.aws:3.0.0 was installed successfully" when you run
    # the above ansible-galaxy command

    # https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-format
    aws configure --profile s3-buckets-security-ansible-project

```

# Pre-requirements - AWS Secret Keys

This project expects AWS credentials cofigured in two approaches.

1. As enviornmental variables
2. As AWS CLI config profile

Set the following environmental variables that are required by inventories/dev-local/group_vars/all.yaml file

1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY
3. AWS_REGION

# Playbooks Execution in this project

## AWS S3 Bucket Create

```sh
./ansible-playbook-wrapper.sh ./playbooks/aws-s3-bucket-create.yaml

```

## AWS S3 Bucket Update Bucket Policies

```sh
./ansible-playbook-wrapper.sh ./playbooks/aws-s3-bucket-update-policies.yaml

```
